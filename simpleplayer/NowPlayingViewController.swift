//
//  NowPlayingViewController.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 4/3/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit
import CoreGraphics
import MediaPlayer

var player:MPMusicPlayerController {return MPMusicPlayerController.systemMusicPlayer()}

var sliderImage:UIImage {
    
    let side = 6
    let rect = CGRect.init(x:0,y:0,width:side,height:side)
    
    let renderer = UIGraphicsImageRenderer.init(size:rect.size)
    
    let img = renderer.image {
        ctx in
        ctx.cgContext.setFillColor(UIColor.white.cgColor)
        ctx.cgContext.addEllipse(in:rect)
        ctx.cgContext.drawPath(using:.fillStroke)
    }
    return img
}

class NowPlayingViewController: UIViewController {
    
    @IBOutlet weak var replayModeButton: UIButton!
    @IBOutlet weak var coverView: UIImageView!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    
    var nowPlayingLabel:UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        return l
    }()
    
    weak var progressTimer:Timer?

    var currentItem:MPMediaItem? { return player.nowPlayingItem }
    
    deinit{
        print("DEALLOC \(self)")
        player.endGeneratingPlaybackNotifications()
        NotificationCenter.default.removeObserver(self)
    }
    
    init(){
        super.init(nibName: "NowPlayingViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let theme = DefaultTheme()
        self.coverView.addSubview(nowPlayingLabel)
        self.nowPlayingLabel.backgroundColor = theme.playPageBackgroundColor.withAlphaComponent(0.66)
        self.view.backgroundColor = theme.playPageBackgroundColor
        nowPlayingLabel.textColor = theme.playPageFontColor
        
        progressSlider.setThumbImage(sliderImage, for: .normal)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissFromSwipe))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)

        if let item = currentItem {
            self.nowPlayingLabel.text = item.title
            
            if let i = item.artwork?.image(at: CGSize(width: 200, height: 200)) {
                coverView.image = i
            }
            
            switch player.playbackState {
            case .stopped, .paused:
                player.setQueue(with: MPMediaItemCollection(items: [item]))
                player.prepareToPlay()
                progressSlider.setValue(0, animated: false)
            default: break
            }
        }
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTappedArtwork))
        doubleTap.numberOfTapsRequired = 2
        coverView.isUserInteractionEnabled = true
//        coverView.addGestureRecognizer(doubleTap) //broken
        
        startProgressTimer()
        
        playPauseButton.addTarget(self, action: #selector(NowPlayingViewController.tappedPlayPause(_:)), for: .touchUpInside)
        rewindButton.addTarget(self, action: #selector(NowPlayingViewController.tappedRewind(_:)), for: .touchUpInside)
        forwardButton.addTarget(self, action: #selector(NowPlayingViewController.tappedFastFwd(_:)), for: .touchUpInside)
        
        registerForPlayerNotifications()
        player.beginGeneratingPlaybackNotifications()
        
        replayModeButton.addTarget(self, action: #selector(didTapReplayMode(sender:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateRepeatModeTitle()
        updateTrackProgress()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let height = 60 as CGFloat
        var frame = self.coverView.bounds
        frame.origin.y += frame.size.height-height
        frame.size.height = height
        nowPlayingLabel.frame = frame
    }
}
    
extension NowPlayingViewController{

    func didTapReplayMode(sender:UIButton){
        
        let vc = UIAlertController.init(title: "Replay Mode", message: nil, preferredStyle: .alert)
        
        let repeatAlbum = UIAlertAction.init(title: "Repeat Album", style: .default) { [unowned self] (action) in
            player.repeatMode = .all
            self.updateRepeatModeTitle()
        }
        
        vc.addAction(repeatAlbum)
        
        let repeatSong = UIAlertAction.init(title: "Repeat Song", style: .default) { [unowned self] (action) in
            player.repeatMode = .one
            self.updateRepeatModeTitle()
        }

        vc.addAction(repeatSong)
        
        let repeatNone = UIAlertAction.init(title: "Repeat Off", style: .default) { [unowned self] (action) in
            player.repeatMode = .none
            self.updateRepeatModeTitle()
        }

        vc.addAction(repeatNone)
        
        self.present(vc, animated: true) { 
            
        }
    }
    
    func updateRepeatModeTitle(){
        switch player.repeatMode {
        case .all:
            self.replayModeButton?.setTitle("repeating album", for: .normal)
        case .one:
            self.replayModeButton?.setTitle("repeating song", for: .normal)
        case .none:
            self.replayModeButton?.setTitle("repeat off", for: .normal)
        case .default:
            self.replayModeButton?.setTitle("default repeat", for: .normal)
        }
    }
    
    func playbackStateDidChange(_ note:Notification){
//        print(note)
    }
    
    func nowPlayingItemDidChange(_ note:Notification){
        print(note)
        if let item = currentItem {
            self.nowPlayingLabel.text = item.title
            
            if let i = item.artwork?.image(at: CGSize(width: 200, height: 200)) {
                coverView.image = i
            }
            
            switch player.playbackState {
            case .playing:
                updateTrackProgress()
            default: break
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //timer keeps a strong reference to its target
        //invalidate it to prevent retain cycle
        if let t = progressTimer{
            t.invalidate()
            progressTimer = nil
        }
    }
    
    func startProgressTimer(){
        
        if let t = progressTimer {
            t.invalidate()
        }
        
        progressTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTrackProgress), userInfo: nil, repeats: true)
    }
    
    func updateTrackProgress(){
        
        let currTime = player.currentPlaybackTime
        if let trackDur = player.nowPlayingItem?.playbackDuration {
            let pct = Float(currTime / trackDur)
            
            progressSlider.setValue(pct, animated: true)
        }
    }
    
    func registerForPlayerNotifications(){
        
        let nc = NotificationCenter.default
        
        nc.addObserver(self, selector: #selector(playbackStateDidChange(_:)), name: NSNotification.Name.MPMusicPlayerControllerPlaybackStateDidChange, object: nil)
        
        nc.addObserver(self, selector: #selector(nowPlayingItemDidChange(_:)), name: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil)
        
        // Posted when the playback state changes, either programatically or by the user.
//        public let MPMusicPlayerControllerPlaybackStateDidChangeNotification: String
//        
//        // Posted when the currently playing media item changes.
//        public let MPMusicPlayerControllerNowPlayingItemDidChangeNotification: String
//        
//        // Posted when the current volume changes.
//        public let MPMusicPlayerControllerVolumeDidChangeNotification: String
    }
    
    func dismissFromSwipe(){
        self.presentingViewController?.dismiss(animated: true, completion: { 
            
        })
    }
    
    func tappedPlayPause(_ sender:UIButton){
        
        togglePlayback()
        
        if !(player.currentPlaybackRate > 0) {
            sender.setTitle("pause", for: .normal)
        } else {
            sender.setTitle("play", for: .normal)
        }
    }
    
    func togglePlayback(){
        if player.currentPlaybackRate > 0 {
            player.pause()
        } else {
            player.play()
        }
    }
    
    func tappedRewind(_ sender:UIButton){
        player.skipToBeginning()
    }
    
    func tappedFastFwd(_ sender:UIButton){
        player.skipToNextItem()
    }
    
    func doubleTappedArtwork(){
        if let id = currentItem?.persistentID {
            let vc = SongViewController(albumPersistentID: id)
            self.present(vc, animated: true, completion: {
                
            })
        }
    }
}
