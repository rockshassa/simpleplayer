//
//  AlbumViewController.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 3/24/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import MediaPlayer

class AlbumViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, ReachabilityEnabledQueryController {
    
    var currentQuery = MPMediaQuery.albums()
    var tableView: UITableView = UITableView()
    let artistID:UInt64
    let imageCache:NSCache<NSString, AnyObject> = NSCache()

    init(artistPersistentID:UInt64){
        self.artistID = artistPersistentID
        super.init(nibName: nil, bundle: nil)
        self.title = "albums"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureAppearance()
        
        self.listenForReachability()
        self.setRightNavNowPlaying()
        
        let pred = MPMediaPropertyPredicate(value: NSNumber(value: artistID as UInt64), forProperty: MPMediaItemPropertyArtistPersistentID, comparisonType: .equalTo)
        currentQuery.addFilterPredicate(pred)
        
        self.title = currentQuery.items?.first?.artist
        
        tableView.backgroundColor = DefaultTheme().tableBackgroundColor
        
        tableView.register(ImageCell.nib, forCellReuseIdentifier:reuseIdentifier)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = tableViewHeight
        
        self.view.addSubview(tableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame = self.view.bounds
    }
    let placeholder = placeholderImage(osize: CGSize(width: tableViewHeight, height: tableViewHeight))
}

private let tableViewHeight:CGFloat = 100

extension AlbumViewController {
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:reuseIdentifier, for: indexPath) as! ImageCell
        
        let a = itemFor(indexPath: indexPath)
        
        if let title = a.albumTitle {
            cell.artistLabel?.text = title
        } else {
            cell.artistLabel?.text = "<no title data>"
        }
        
        if let img = imageCache.object(forKey: "\(a.albumPersistentID)" as NSString) as? UIImage {
            cell.artistImage?.image = img
        } else if (a.artwork != nil), let img = a.artwork!.image(at: CGSize(width: tableViewHeight, height: tableViewHeight)) {
            imageCache.setObject(img, forKey: "\(a.albumPersistentID)" as NSString)
            cell.artistImage?.image = img
        } else {
            cell.artistImage?.image = placeholder
        }
        
        return cell
    }
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let a = itemFor(indexPath: indexPath)
        
        let vc = SongViewController(albumPersistentID: a.albumPersistentID)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
