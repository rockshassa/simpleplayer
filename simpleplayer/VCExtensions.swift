//
//  VCExtensions.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 4/10/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import MediaPlayer

protocol ReachabilityEnabledQueryController {
    var currentQuery:MPMediaQuery { get }
    var tableView:UITableView { get }
}

extension ReachabilityEnabledQueryController {
    
    var reuseIdentifier:String {
        let ri = NSStringFromClass(type(of:self) as! AnyClass)
//        print(ri)
        return ri
    }
    
    func itemFor(indexPath:IndexPath)->MPMediaItem{
        
        let sec = currentQuery.collectionSections![indexPath.section]
        
        let b = currentQuery.collections![sec.range.location+indexPath.row]
        let a = b.items.first!
        
//        print("\(indexPath.section) \(indexPath.row) \(a.artist) \(a.title)")
        
        return a
    }
}

/*
 This extension needs to live on UIViewController instead of ReachabilityEnabledQueryController
 This is because UITableViewDataSource requires @objc bridging, which cant be done from a swift protocol
 */
extension UIViewController {
    
    var querySelf:ReachabilityEnabledQueryController {
        return self as! ReachabilityEnabledQueryController
    }
    
    @objc(numberOfSectionsInTableView:) func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = querySelf.currentQuery.collectionSections {
            
            var count = 0
            for sec in sections {
                if sec.range.length > 0 {
                    count += 1
                }
            }
            
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = querySelf.currentQuery.collectionSections {
            var nonZeroSections = [MPMediaQuerySection]()
            for sec in sections {
                if sec.range.length > 0 {
                    nonZeroSections.append(sec)
                }
            }
            
            let sec = nonZeroSections[section]
            return sec.range.length
        }
        return 0
    }
    
    func listenForReachability() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.reachabilityChanged(_:)), name:ReachabilityChangedNotification, object: nil)
        
        handleReachability(Reachability.isConnectedToNetwork())
    }
    
    
    func handleReachability(_ reach:Bool){
        if let queryController = self as? ReachabilityEnabledQueryController {
            
            let query = queryController.currentQuery
            
            query.configureReachability()
            
            DispatchQueue.main.async(execute: {
                
                queryController.tableView.reloadData()
                
                if query.items?.count == 0 &&
                    self.navigationController?.viewControllers.last == self {
                    let _ = self.navigationController?.popToRootViewController(animated: true)
                }
            })
        }
    }
    
    func reachabilityChanged(_ note:Notification){
        print(note)
        
        handleReachability(note.reach())
    }
    
    func configureAppearance(){
        let theme = DefaultTheme()
        
        querySelf.tableView.backgroundColor = UIColor.clear
        
        view.backgroundColor = theme.tableBackgroundColor
        
        querySelf.tableView.sectionIndexBackgroundColor = UIColor.clear
        querySelf.tableView.sectionIndexColor = UIColor.white
//        querySelf.tableView.sectionIndexColor
    }
}

extension UIViewController {
    
    func setRightNavNowPlaying(){
        
        let button = UIBarButtonItem(title: "now playing", style: .plain, target: self, action: #selector(tappedNowPlaying))
        self.navigationItem.rightBarButtonItem = button
    }
    
    func tappedNowPlaying(){
        
        let vc = NowPlayingViewController()
        self.present(vc, animated: true, completion: {
            
        })
        
    }    
}

func placeholderImage(osize:CGSize)->UIImage{
    
//    let size = CGSize(width: osize.width*2, height: osize.height*2)
    let size = osize
    
    let label = UILabel()
    label.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    label.textAlignment = .center
    label.text = "SP"
    label.font = UIFont.boldSystemFont(ofSize: 20)
    
    label.backgroundColor = UIColor(red: 0.1, green: 0.5, blue: 0.1, alpha: 1)
    label.textColor = UIColor.white

    UIGraphicsBeginImageContext(size)
    
    label.draw(label.layer, in: UIGraphicsGetCurrentContext()!)
    
    let img = UIGraphicsGetImageFromCurrentImageContext()
    
    UIGraphicsEndImageContext()
    
    return img!
}


extension MPMediaQuery {
    func configureReachability(){
        
        self.removeFilterPredicate(offlinePredicate())
        
        if !Reachability.isConnectedToNetwork() {
            self.addFilterPredicate(offlinePredicate())
        }
    }
    
    func offlinePredicate() -> MPMediaPropertyPredicate {
        return MPMediaQuery.offlinePredicate()
    }
    
    class func offlinePredicate() -> MPMediaPropertyPredicate {
        return MPMediaPropertyPredicate(value: NSNumber(value: false as Bool), forProperty: MPMediaItemPropertyIsCloudItem)
    }
}
