//
//  ImageCell.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 10/23/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit

class ImageCell : UITableViewCell, Nib {
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistLabel: UILabel!
    
    var firstView = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        
        if (firstView){
            firstView = false
            let imgWidth = NSLayoutConstraint.init(item: contentView, attribute: .height, relatedBy: .equal, toItem: artistImage, attribute: .width, multiplier: 1, constant: 0)
            contentView.addConstraint(imgWidth)
            
            applyTheme()
            self.backgroundColor = UIColor.clear
        }
        
        super.willMove(toSuperview: newSuperview)
    }
    
    func applyTheme(){
        let t = DefaultTheme()
        artistLabel.font = t.imageCellLabelFontPrimary
        artistLabel.textColor = t.labelColorPrimary
    }
    
    override func prepareForReuse() {
        artistImage.image = nil
        artistLabel.text = nil
        super.prepareForReuse()
    }
}
