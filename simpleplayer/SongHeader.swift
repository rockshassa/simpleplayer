//
//  SongHeader.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 11/19/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit

class SongHeader : UIView {
    
    var iCloudButton = UIButton(type: .roundedRect)
    
    required init() {
        
        super.init(frame: CGRect.zero)
        
        self.addSubview(iCloudButton)
        iCloudButton.setTitle("btn", for: .normal)
        iCloudButton.sizeToFit()
        
        let c1 = NSLayoutConstraint.init(item: iCloudButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -10)
        
        let c2 = NSLayoutConstraint.init(item: iCloudButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 200)
        
        let c3 = NSLayoutConstraint.init(item: iCloudButton, attribute: .centerYWithinMargins, relatedBy: .equal, toItem: self, attribute: .centerYWithinMargins, multiplier: 1, constant: 0)
        
        self.addConstraints([c1,c2,c3])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
