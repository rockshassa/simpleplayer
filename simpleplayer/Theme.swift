//
//  Theme.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 9/25/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit

protocol Theme {
    var labelColorPrimary:UIColor {get}
    var tableBackgroundColor:UIColor {get}
    var textCellLabelFontPrimary:UIFont {get}
    var imageCellLabelFontPrimary:UIFont {get}
}

class DefaultTheme:Theme{
    //table cell font
    var textCellLabelFontPrimary: UIFont {
        let font = UIFont.preferredFont(forTextStyle: .body)
        return font
    }
    
    var imageCellLabelFontPrimary: UIFont {
        let font = UIFont.preferredFont(forTextStyle: .headline)
        return font
    }
    
    //table cell text color
    let labelColorPrimary: UIColor = UIColor.white
    
    //table BG color
    let tableBackgroundColor:UIColor = UIColor.black
    
    //play page BG color
    let playPageBackgroundColor:UIColor = UIColor.black
    
    //play page font color
    let playPageFontColor:UIColor = UIColor.white
    
    //play page control color
    let playPageControlColor:UIColor = UIColor.green
}

extension DefaultTheme {
    
//    func
}
