//
//  ViewController.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 3/22/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit
import MediaPlayer

class ArtistsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ReachabilityEnabledQueryController {

    let tableView = UITableView()
    var currentQuery:MPMediaQuery = {
        let q = MPMediaQuery.artists()
        q.groupingType = .artist
        return q
    }()
    
    var imageCache:NSCache<NSString, AnyObject> = NSCache()
    let placeholder = placeholderImage(osize: CGSize(width: tableViewHeight, height: tableViewHeight))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setRightNavNowPlaying()
        
        self.listenForReachability()
        self.configureAppearance()
        
        self.view.addSubview(tableView)
        
        title = "artists"
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.backgroundColor = DefaultTheme().tableBackgroundColor
        
        tableView.register(ImageCell.nib, forCellReuseIdentifier: reuseIdentifier)
        tableView.register(SectionHeaderView.nib, forHeaderFooterViewReuseIdentifier: headerReuse)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = tableViewHeight
        
        
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = self.view.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
}

private let tableViewHeight:CGFloat = 80

var titles:[String]!

extension ArtistsViewController {

    var headerReuse:String { return "header" }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerReuse) as! SectionHeaderView
        if (titles == nil){
            titles = self.sectionIndexTitles(for: tableView)
        }
        view.label.text = titles[section]
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30 as CGFloat
    }
    
    @objc(sectionIndexTitlesForTableView:) func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        guard let sections = currentQuery.collectionSections else {
            return nil
        }
        
        var titles:[String] = []
        for sec in sections {
            
            if let char = sec.title.characters.first, sec.range.length > 0 {
                let title = "\(char)"
                titles.append(title)
            }
        }
        
        return titles
    }
        
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ImageCell
        
        let a = itemFor(indexPath: indexPath)
        
        if let artist = a.artist {
            cell.artistLabel?.text = artist
        } else {
            cell.artistLabel?.text = "<no artist data>"
        }
        
        if let img = imageCache.object(forKey: "\(a.artistPersistentID)" as NSString) as? UIImage {
            cell.artistImage?.image = img
        } else if (a.artwork != nil), let img = a.artwork!.image(at: CGSize(width: tableViewHeight, height: tableViewHeight)) {
          
            imageCache.setObject(img, forKey: "\(a.artistPersistentID)" as NSString)
            cell.artistImage?.image = img
        
        } else {
            cell.artistImage?.image = placeholder
        }
        
        return cell
    }
    
    
    
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewHeight
    }
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let obj = itemFor(indexPath: indexPath)
        let artistId = obj.artistPersistentID
        
        let albumList = albumListForArtist(artistId)
        
        if (albumList.count == 1){
            let albumID = albumList.first!.albumPersistentID
            let songvc = SongViewController(albumPersistentID: albumID)
            self.navigationController?.pushViewController(songvc, animated: true)
        } else {
            let vc = AlbumViewController(artistPersistentID: artistId)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func albumListForArtist(_ artistID:UInt64) -> [MPMediaItem] {
        
        let pred = MPMediaPropertyPredicate(value: NSNumber(value: artistID as UInt64), forProperty: MPMediaItemPropertyArtistPersistentID, comparisonType: .equalTo)
        
        let query = MPMediaQuery.albums()
        
        query.addFilterPredicate(pred)
        query.configureReachability()
        
        var albums:[MPMediaItem] = []
        
        for i in query.items! {
            
            if albums.count == 0 {
                albums.append(i)
            }
            
            if albums.last!.albumPersistentID != i.albumPersistentID {
                albums.append(i)
            }
        }
        return albums
    }
}
