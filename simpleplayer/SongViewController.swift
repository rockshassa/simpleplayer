//
//  SongViewController.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 4/3/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation
import MediaPlayer
import UIKit

class SongViewController : UIViewController, UITableViewDelegate, UITableViewDataSource, ReachabilityEnabledQueryController {
    
    var albumID:UInt64
    let tableView = UITableView()
    var albumImageView = UIImageView()
    var currentQuery = MPMediaQuery.songs()
    
    init(albumPersistentID:UInt64){
        self.albumID = albumPersistentID
        let pred = MPMediaPropertyPredicate(value: NSNumber(value: albumID as UInt64), forProperty: MPMediaItemPropertyAlbumPersistentID, comparisonType: .equalTo)
        currentQuery.addFilterPredicate(pred)
        currentQuery.configureReachability()
        super.init(nibName:nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listenForReachability()
        
        self.configureAppearance()
        
        title = currentQuery.items!.first?.albumTitle
        
        self.setRightNavNowPlaying()
        
        self.view.addSubview(albumImageView)
        
        self.view.addSubview(tableView)
        
        tableView.register(TextCell.nib, forCellReuseIdentifier: reuseIdentifier)
        
        let v = UIView()
        v.backgroundColor = UIColor.clear
        tableView.backgroundView = v
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        albumImageView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.maxY, width: self.view.frame.size.width, height: self.view.frame.size.width)
        
        if let img = currentQuery.items!.first?.artwork?.image(at: albumImageView.frame.size) {
            albumImageView.image = img
        } else {
            let img = placeholderImage(osize: albumImageView.frame.size)
            albumImageView.image = img
        }
        
        tableView.frame = self.view.bounds
        
        tableView.contentInset = UIEdgeInsetsMake(albumImageView.frame.maxY, 0, 0, 0)
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.navigationController!.navigationBar.frame.maxY, 0, 0, 0)
        tableView.backgroundColor = UIColor.clear

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        tableView.contentOffset = CGPoint(x: 0, y: -albumImageView.frame.maxY)
    }
    
    func didTapICloud(sender:UIButton){
        
    }
}


extension SongViewController {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if !Reachability.isConnectedToNetwork(){
            return 0
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SongHeader()
        
        view.iCloudButton.removeTarget(nil, action: nil, for: .allEvents)
        
        view.iCloudButton.addTarget(self, action: #selector(didTapICloud(sender:)), for: .touchUpInside)
        
        return view;
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! TextCell
        
        let item = itemFor(indexPath: indexPath)
        
        cell.configureFor(item: item, index: indexPath)
        
        return cell
    }
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let t = itemFor(indexPath: indexPath)
        setPlayerContentIfNeeded(t, queue: currentQuery.items!)
        
        let vc = NowPlayingViewController()
        
        self.navigationController?.present(vc, animated: true) {
            
        }
    }
    
    func setPlayerContentIfNeeded(_ item:MPMediaItem, queue:[MPMediaItem]){
        
        var descString = "Set player queue to:\n"
        for i in queue{
            descString+="\(i.title)\n"
        }
        
        print(descString)
        
        let collection = MPMediaItemCollection(items: queue)
        let p = player
        
        if item.persistentID != p.nowPlayingItem?.persistentID {
            p.setQueue(with: collection)
            p.nowPlayingItem = item
            p.play()
        }
    }
}



