//
//  ArtistCell.swift
//  simpleplayer
//
//  Created by Nicholas Galasso on 9/24/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import UIKit
import MediaPlayer

class TextCell : UITableViewCell, Nib {
    
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    var firstView = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        
        if (firstView){
            firstView = false
            applyTheme()
        }
        super.willMove(toSuperview: newSuperview)
    }
    
    func applyTheme(){
        let theme = DefaultTheme()
        
        for l:UILabel in [numberLabel, songLabel] {
            l.font = theme.textCellLabelFontPrimary
            l.textColor = theme.labelColorPrimary
            let bgColor = DefaultTheme().tableBackgroundColor.withAlphaComponent(0.75)
            self.contentView.backgroundColor = bgColor
            l.clipsToBounds = false
        }
        self.backgroundColor = UIColor.clear
        //        self.adjustsFontSizeToFitWidth = true
    }
    
    override func prepareForReuse() {
        songLabel.text = nil
        super.prepareForReuse()
    }
    
    func configureFor(item:MPMediaItem,index:IndexPath){
        
        let num = index.row+1
        numberLabel.text = "\(num)."
        
        if let title = item.title {
            songLabel?.text = title
        } else {
            songLabel?.text = "<no song data>"
        }
    }
}

protocol Nib {
    static var nib:UINib {get}
}

extension Nib {
    static var nib:UINib {
        let type = "\(type(of:self))".components(separatedBy: ".").first
        return UINib(nibName:type!, bundle: nil)
    }
}

class SectionHeaderView:UITableViewHeaderFooterView, Nib {
    
    @IBOutlet weak var label: UILabel!
    
    override func prepareForReuse() {
        label.text = nil
        super.prepareForReuse()
    }
}
